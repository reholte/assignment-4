import { Component } from "@angular/core";

@Component({
    selector: 'app-pokemons-collected', 
    templateUrl: './pokemons-collected.component.html', 
    styleUrls: ['./pokemons-collected.component.css']
}) 
export class PokemonsCollectedComponent{

    public getName() : string {
        return JSON.parse(localStorage.name); 
    }
    public getPokemons(): string[] {
        let pokemons = JSON.parse(localStorage.pokemons); 
        let pokemonNames = []; 
        if (pokemons !== null){
            for (let pokemon of pokemons){
                pokemonNames.push(pokemon.name)
            }
        }
        return pokemonNames; 
    }
}