import { Component } from "@angular/core";
import { Pokemon } from "../models/pokemons.model";
import { SelectedPokemonService } from "../services/selected-pokemon.service";

@Component({
    selector: 'app-pokemon-selected', 
    templateUrl: './pokemon-selected.component.html',
    styleUrls: ['./pokemon-selected.component.css']
})

export class PokemonSelectedComponent{
    constructor(private readonly selectedPokemonService: SelectedPokemonService){   
    }

    get pokemon(): Pokemon | null {
        let pokemon = this.selectedPokemonService.pokemon(); 
        if (pokemon !== null){
            let pokemons = JSON.parse(localStorage.pokemons);
            let pokemonNames = []; 
            for (let p of pokemons){
                pokemonNames.push(p.name)
            }
            if (!pokemonNames?.includes(pokemon.name)){
                pokemons.push(pokemon);
                localStorage.setItem("pokemons", JSON.stringify(pokemons)); 
            }
        }
        return this.selectedPokemonService.pokemon();
    }
}