import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.components';
import { PokemonSelectedComponent } from './pokemon-selected/pokemon-selected.component';
import { PokemonListItemComponent } from './pokemon-list-item/pokemon-list-item.component';

import { CataloguePage } from './catalogue/catalogue.page';
import { LandingPage } from './landing/landing.page';

import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { NotFoundPage } from './not-found/not-found.page';
import { TrainerPage } from './trainer/trainer.page';
import { PokemonsCollectedComponent } from './pokemons-collected/pokemons-collected.component';

@NgModule({ //Decorator
  declarations: [
    AppComponent,
    PokemonListComponent, 
    PokemonSelectedComponent, 
    PokemonListItemComponent, 
    CataloguePage,
    LandingPage, 
    NavbarComponent, 
    NotFoundPage, 
    TrainerPage, 
    PokemonsCollectedComponent
  ],
  imports: [
    BrowserModule, 
    HttpClientModule, 
    AppRoutingModule, 
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
