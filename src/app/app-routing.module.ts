import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CataloguePage } from "./catalogue/catalogue.page";
import { LandingPage } from "./landing/landing.page";
import { NotFoundPage } from "./not-found/not-found.page";
import { TrainerPage } from "./trainer/trainer.page";

// login
// catalogue
// training 

const routes: Routes = [
    {
        path: '', 
        pathMatch: 'full',
        redirectTo: '/landing'
    },
    {
        path: 'catalogue', 
        component: CataloguePage
    }, 
    {
        path: 'landing', 
        component: LandingPage
    }, 
    {
        path: 'trainer', 
        component: TrainerPage
    }, 
    {
        path: '**' ,
        component: NotFoundPage
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ], 
    exports: [ RouterModule ]
})

export class AppRoutingModule {} 