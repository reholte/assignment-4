import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Pokemon, PokemonObject } from "../models/pokemons.model";

@Injectable({
    providedIn: 'root'
})
export class PokemonsService { 
    private _pokemons: Pokemon[] = []; 
    private _pokemonObject: PokemonObject = {count: 0, next: null, previous: null, results: this._pokemons};
    private _error: string = '';

    // DI - Dependency Injection
    constructor(private readonly http: HttpClient) {
    }

    public fetchPokemons(): void {
        this.http.get<PokemonObject>('https://pokeapi.co/api/v2/pokemon?offset=0&limit=151')
            .subscribe((pokemonObject: PokemonObject) => {
                this._pokemonObject = pokemonObject;
            }), (error: HttpErrorResponse) => {
                this._error = error.message;
            }
    }


    public pokemonObject(): PokemonObject {
        return this._pokemonObject; 
    }

    public error(): string {
        return this._error;
    }
    public pokemons(): Pokemon[] {
        let pokemons = this._pokemonObject.results; 
        for (let pokemon of this._pokemons){
            let urlReverse = this.reverseString(pokemon.url);
            let number = ""; 
            for (let i = 1; i < urlReverse.length; i++){
                let char = urlReverse[i]
                if (char === "/"){
                    break; 
                }
                number += char 
            }
            let id = this.reverseString(number);
            pokemon.img = "https://raw.githubusercontent.com/PokeAPI/sprites/84204f8594790cfd04190a8d82beb31f49115c02/sprites/pokemon/"+ id + ".png";;
        }
        this._pokemons = pokemons;
        return this._pokemons; 
    }

    public reverseString(str: string): string{
        let splitString = str.split(""); 
        let reverseArray = splitString.reverse();
        let joinArray = reverseArray.join("");
        return joinArray; 
    }
}