import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector: 'app-landing', 
    templateUrl: './landing.page.html', 
    styleUrls: ['./landing.page.css']
})


export class LandingPage {
    constructor(private router: Router) { } 
    public onSubmit(createForm: NgForm): void {
        localStorage.setItem('name',JSON.stringify(createForm.value.name));
        localStorage.setItem('pokemons', JSON.stringify([]))
        this.router.navigate(['/catalogue']);
    }
}