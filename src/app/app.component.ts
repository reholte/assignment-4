import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pokemon';
  constructor(private router: Router) { }

  ngOnInit(): void{
    if (localStorage.getItem("name") !== null){
      this.router.navigate(['/catalogue']);
  }
  else {
      this.router.navigate(['/landing']);
  }
  }
}
