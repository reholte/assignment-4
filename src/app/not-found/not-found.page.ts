import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'app-not-found', 
    templateUrl: './not-found.page.html', 
    styleUrls: ['./not-found.page.css']
})


export class NotFoundPage {
    constructor(private router: Router) { }

    public onLoginSubmit(): void {
        this.router.navigate(['/landing']);
    }
}