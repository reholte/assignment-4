import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector: 'app-navbar', 
    templateUrl: './navbar.component.html', 
    styleUrls: ['./navbar.component.css']
})


export class NavbarComponent {
    constructor(private router: Router) { }
    public onCatalogueSubmit(): void { 
        if (localStorage.getItem("name") !== null){
            this.router.navigate(['/catalogue']);
        }
        else {
            this.router.navigate(['/landing']);
        }
    }
    public onTrainerSubmit(): void {
        if (localStorage.getItem("name") !== null){
            this.router.navigate(['/trainer']);
        }
        else {
            this.router.navigate(['/landing']);
        }
    }
    public onLogoutSubmit(): void {
        localStorage.clear()
        this.router.navigate(['/landing']);
    }
}