import { Component, OnInit } from "@angular/core";
import { Pokemon, PokemonObject } from "../models/pokemons.model";
import { PokemonsService } from "../services/pokemons.service";
import { SelectedPokemonService } from "../services/selected-pokemon.service";

@Component({
    selector: 'app-pokemon-list', 
    templateUrl: './pokemon-list.component.html', 
    styleUrls: ['./pokemon-list.component.css']
}) //Decorator 
export class PokemonListComponent implements OnInit {
    constructor(private readonly pokemonsService: PokemonsService, private readonly selectedPokemonService: SelectedPokemonService) {
    }

    ngOnInit(): void {
        this.pokemonsService.fetchPokemons(); 
    }

    get pokemonObject(): PokemonObject {
        return this.pokemonsService.pokemonObject(); 
    }


    get pokemons(): Pokemon[] {
        return this.pokemonsService.pokemons(); 
    }

    public handlePokemonClicked(pokemon: Pokemon): void {
        this.selectedPokemonService.setPokemon(pokemon); 
    }

}